using Test
using TestItemRunner

@testitem "Maximum number of iterations" tags=[:unit] begin
	include("Implementations/MaximumIterations.jl")
end

@run_package_tests filter=ti->(:unit in ti.tags)

