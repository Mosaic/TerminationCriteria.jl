using Test
using TerminationCriteria: query, maximumiterations

# Define function with a loop that should early terminate
function earlybird(n, maxiter)
	# iterate with implicit termination s.t. test can fail if necessary
	for _ in 1:n
		# query criterion to early break
		query(maxiter; printinfo=false) && return true
	end
	# otherwise return false to indicate failure of test
	false
end

# Define number of maximum iterations
n = 10

# Test if termination was done implicitly or explicitly by criterion
@test earlybird(n, maximumiterations(n))

# Test broken test on purpose
@test_broken earlybird(n, maximumiterations(n+1))