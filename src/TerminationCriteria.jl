# Copyright (C) 2024 Dominik Itner
# TerminationCriteria.jl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# TerminationCriteria.jl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with TerminationCriteria.jl.  If not, see <http://www.gnu Lesser.org/licenses/>.
# A copy of the GNU Lesser General Public License has deliberately not been included to circumvent unnecessary traffic due to the high degree of modularity and interoperability of packages in Mosaic.jl.
# The license can instead be found at <https://codeberg.org/Mosaic/License>.

module TerminationCriteria

# Supertype

abstract type TerminationCriterion end

# Interface definition

## Internal interface

function message end

## External Interface

function query end

## Global interface implementation

"Evaluate a tuple of termination criteria and return `true` if any criterion is met."
function query(criteria::Tuple{Vararg{TerminationCriterion}}, args...; kwargs...)
	# initialize boolean to accumulate queries
	cumulative = false
	# iterate over criteria
	for criterion in criteria
		# accumulate queries s.t. any true (met) criterion terminates the process
		cumulative = cumulative || query(criterion, args...; kwargs...)
		# lazily break iteration on first `true`
		cumulative && break
	end
	# return cumulative boolean
	cumulative
end

# Implementations

include("Implementations/MaximumIterations.jl")

end # TerminationCriteria