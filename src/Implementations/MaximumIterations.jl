# Maximum iterations

mutable struct MaximumIterations{T} <: TerminationCriterion
	const n::T
	i::T
end

## Instantiate

"Make a `TerminationCondition` based on the maximum number of iterations allowed."
maximumiterations(n::T) where T =
	MaximumIterations{T}(n, zero(T))

## Extend inteface

message(criterion::MaximumIterations) =
	printstyled("\rReached maximum number of iterations: $(criterion.n).\n", color=:cyan)

function query(criterion::MaximumIterations, ::Vararg; printinfo=false, kwargs...)
	# evaluate status of criterion and modify if necessary
	status = (criterion.i = criterion.i + 1) ≥ criterion.n
	# short-circuit message
	printinfo && status && message(criterion)
	# return status
	status
end